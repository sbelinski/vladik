const http = require('http');
const fs = require('fs');
const app = require('./app');

http.createServer(app.handleRequest).listen(3000);