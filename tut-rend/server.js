const http = require('http');
const fs = require('fs');

function onRequest(request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('./index.html', null, function (error, data) {
            if(error) {
            response.writeHead(404);
            response.write('Page not found');
        } else {
            response.write(data);
        }
        response.end();
    })
}

http.createServer(onRequest).listen(3000);